
// ###
// # customise config here
// number of pixels per tile e.g. 5x5 or 10x10
const resolution = 5;

// total size of grid in pixels e.g. 700 x 700
const size = 800;

const ruleSet = "B6/S16"; // traditional is "B3/S23", "B36/S23"
// # customise config end 
// ###

let canvas;
let grid;
let cols;
let rows;

function setup() {
  canvas = createCanvas(size, size);

  canvas.parent("root");

  cols = width / resolution;
  rows = height / resolution;

  grid = initGrid(cols, rows);
}

function draw() {
  background('#222');

  renderGrid(grid, resolution);

  grid = nextGeneration(grid, ruleSet);
}