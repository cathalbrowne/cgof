function initGrid(cols, rows) {
  return seeded2DArray(cols, rows);
}

function seeded2DArray(cols, rows) {
  let array = make2DArray(cols, rows);

  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      array[x][y] = Math.round(Math.random(2));
    }
  }

  return array;
}


function make2DArray(cols, rows) {
  let array = new Array(cols);

  for (let i = 0; i < cols; i++) {
    array[i] = new Array(rows);
  }

  return array;
}

function renderGrid(grid, resolution) {
  const cols = grid.length;
  const rows = grid[0].length;

  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      if (grid[x][y] == 0) continue;

      fill(255);
      noStroke();
      circle(x * resolution, y * resolution, resolution);
    }
  }
}

function nextGeneration(grid, ruleSet = "B3/S23") {
  const cols = grid.length;
  const rows = grid[0].length;

  let newGrid = make2DArray(cols, rows);

  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      const aliveNeighbours = aliveNeighbourCount(grid, x, y);

      if (ruleSet == "B3/S23") {
        if (grid[x][y] == 1 && (aliveNeighbours < 2 || aliveNeighbours > 3)) {
          newGrid[x][y] = 0;
        } else if (grid[x][y] == 1 && (aliveNeighbours == 2 || aliveNeighbours == 3)) {
          newGrid[x][y] = 1;
        } else if (grid[x][y] == 0 && aliveNeighbours == 3) {
          newGrid[x][y] = 1;
        }
        else {
          newGrid[x][y] = grid[x][y];
        }
      }
      else if (ruleSet == "B6/S16") {
        if (grid[x][y] == 1 && (aliveNeighbours < 1 || aliveNeighbours > 6)) {
          newGrid[x][y] = 0;
        } else if (grid[x][y] == 1 && (aliveNeighbours == 1 || aliveNeighbours == 6)) {
          newGrid[x][y] = 1;
        } else if (grid[x][y] == 0 && aliveNeighbours == 6) {
          newGrid[x][y] = 1;
        }
        else {
          newGrid[x][y] = grid[x][y];
        }
      }
      else if (ruleSet == "B36/S23") {
        if (grid[x][y] == 1 && (aliveNeighbours < 2 || aliveNeighbours > 3)) {
          newGrid[x][y] = 0;
        } else if (grid[x][y] == 1 && (aliveNeighbours == 2 || aliveNeighbours == 3)) {
          newGrid[x][y] = 1;
        } else if (grid[x][y] == 0 && (aliveNeighbours == 3 || aliveNeighbours == 6)) {
          newGrid[x][y] = 1;
        }
        else {
          newGrid[x][y] = grid[x][y];
        }
      }
      else {
        throw "ruleSet(" + ruleSet + ") not recognised";
      }
    }
  }

  return newGrid;
}

function aliveNeighbourCount(grid, x, y) {
  let count = 0;

  const cols = grid.length;
  const rows = grid[0].length;

  for (let i = -1; i < 2; i++) {
    for (let j = -1; j < 2; j++) {
      let searchX = (x + i + cols) % cols;
      let searchY = (y + j + rows) % rows;

      if (searchX == x && searchY == y) continue;

      count += grid[searchX][searchY];
    }
  }

  return count;
}


